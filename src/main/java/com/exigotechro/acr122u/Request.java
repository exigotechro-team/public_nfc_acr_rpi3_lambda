package com.exigotechro.acr122u;

public class Request
{
    String deviceId;
    String tagId;
    String terminal;
    String terminal_sn;

    public Request() {}

    public Request(String _deviceId, String _tagId, String _terminal, String _terminal_sn ) {
        this.deviceId = _deviceId;
        this.tagId = _tagId;
        this.terminal = _terminal;
        this.terminal_sn = _terminal_sn;
    }

    public String getDeviceId() {
        return deviceId;
    }
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getTagId() {
        return tagId;
    }
    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public String getTerminal() { return terminal; }
    public void setTerminal(String terminal) { this.terminal = terminal; }

    public String toString(){
        return String.format("DeviceID: %s / TagId: %s / Terminal: %s / Terminal SN: %s", this.deviceId, this.tagId, this.terminal, this.terminal_sn);
    }

    public String getTerminal_sn() {return terminal_sn;}
    public void setTerminal_sn(String terminal_sn) { this.terminal_sn = terminal_sn; }

}
