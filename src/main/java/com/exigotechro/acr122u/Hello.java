package com.exigotechro.acr122u;

import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.Context;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Hello
        implements RequestHandler<Request, Response>
{
    LambdaLogger logger;

    @Override
    public Response handleRequest(Request input, Context context)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        String timestamp = dateFormat.format(Calendar.getInstance().getTime());
        String msg = String.format("Lambda>Hello: %s [ %s ]", timestamp, input.toString());

        logger = context.getLogger();
        logger.log(msg);

        return new Response(msg);
    }

}



