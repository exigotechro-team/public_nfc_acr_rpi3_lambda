package com.exigotechro.acr122u;

public class Response {
    String message;

    public Response() {}

    public Response(String _msg) {
        this.message = _msg;
    }
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
